import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('phoebus')


def test_phoebus_installed(host):
    cmd = host.run("/usr/local/bin/phoebus_current -help")
    assert cmd.rc == 0
